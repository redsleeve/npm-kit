# npm--kit

## List of available tasks

### `clean`
  `rm -f assets/{css/*,js/*,images/*}`

  Delete existing assets files

### `autoprefixer`

### `babel`

### `scss`

### `lint`

### `uglify`

### `imagemin`

### `icons`

### `serve`

### `build:css`

### `build:js`

### `build:images`

### `build`

### `watch:css`

### `watch:js`

### `watch:images`

### `watch`

### `postinstall`

### `postinstall`

## My Sublime-Text Settings / Stop Index node_modules folder

"binary_file_patterns": [
  "*.jpg", "*.jpeg", "*.png", "*.gif", "*.ttf", "*.tga", "*.dds",
  "*.ico", "*.eot", "*.pdf", "*.swf", "*.jar", "*.zip",
  "node_modules/**",
  "bower_components/**"
]

